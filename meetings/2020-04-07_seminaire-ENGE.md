Seminaire ENGE
--------------



# Présentation du projet CL


* Apporter des solutions à la crise en trackant la propagation du virus
* Pouvoir sortir du confinement mais seulement si le risque de propagation est contenu
* Proposer une approche horizontale de collecte de données volontairement (VGI, crowdsourcing) plutôt qu'une surveillance étatique ou par des opérateurs privés
* Proposer un développement collaboratif d'une solution open-source (-> Free Software Foundation Europe)

# Etat des lieux

* pas pu développer un POC, seulement au stade de l'idée et qq Implémentation d'une DB
* peu de temps pour dvlp.
* Autres applications en cours de dvlp: se accrocher à d'autres trains en marche?


# Idée

<https://gitlab.com/colibre-19/data-tracking-main>

* don de ses données facilement, en utilisant différentes sources de données


## Technique

* mobilityDB

## Similaire

<http://safepaths.mit.edu/>

<https://covid-initiatives.org/recherche/liste>
