# Trackage épidémiologique

## Introduction

Nous proposons de mettre en place un système où un citoyen se sachant infecté par le COVID-19 puisse faire don de ses données de localisation des 14 derniers jours.

Grâce à de telles données, il sera possible de détecter les zones à haut risque de contagion. Cette information pourra, nous l'espérons, aider à endiguer l'épidémie :

- un citoyen pourra savoir si il a été exposé à un haut risque de contamination
- le pouvoir public pourra adapter sa stratégie dans la lutte contre le virus

## Collecte des données

Il existe plusieurs source de données de localisation :

- facebook
- google
- application existante ou spécialement développée pour ce cas.

La partie collecte des données sera une interface web expliquant comment faire don de ses données.

L'outil ne gardera en mémoire que les données des 21 derniers jours. Il sera aussi demandé à la personne une estimation de sa date d'infection. L'outil stockera les données de manière anonyme. Afin de renforcer l'anonymisation nous pouvons exclure un buffer autour de l'habitation de la personne, ou de son travail.

Proposition de technologie :

- PostGIS
- MobilityDB ( https://github.com/ULB-CoDE-WIT/MobilityDB )
- Django

## Interrogation des données

Une option permettra pour n'importe qui d'envoyer ses données afin de savoir si il a été exposé à un risque de contamination. Dans ce cas, les données soumises ne sont pas stockées. Nous pouvons imaginer qu'une personne pourra argumenter pour se faire tester si il s'avère qu'elle a été exposée à un fort risque de contamination.

Faut-il prévoir un système par sms indiquant à une personne si elle a été exposée à un fort risque ( quand une personne se sachant infectée partage ses données qui "matchent" avec celle de la personne)? Dans ce cas, faut-il garder les données des personnes non infectées avec un moyen de contact (plus d'anonymisation).

Pour les autorités, cela peut permettre d'identifier des personnes à tester.

Un point fort du système est que même si peu de malades partagent leurs données, le système permet d'indiquer des zones à risque et de donner une certaine priorité pour une partie des tests.

## Vie privée

L'utilisation de données de suivi des personnes soulève des questions de vies privées cruciales. Le projet se doit de respecter la législation en vigueur et des normes éthiques les plus hautes possibles.

## Implémentation

- Base de donnée (mobilitydb) : https://gitlab.com/colibre-19/data-tracking-db

## Articles sur le sujet

* ![Revue de presse générale](revue-presse.md)
* ![Private Kit: Safe Paths](safe-paths.md)

## Autres initiatives

* Europe : https://ec.europa.eu/digital-single-market/en/news/join-ai-robotics-vs-covid-19-initiative-european-ai-alliance
